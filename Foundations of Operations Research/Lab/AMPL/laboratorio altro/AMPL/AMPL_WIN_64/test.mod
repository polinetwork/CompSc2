# YOU CAN WRITE COMMENTS

set I;
set J;

param  A  {I ,J};
param  b {J};
param c{J};

var  x {J},>=0 ;

minimize Cost :
sum  {j in J}   c[j]*x[j];

subject to MatrixConstraint { i in I}:
sum {j in J}  A[i,j]*x[j] <= b[i];
