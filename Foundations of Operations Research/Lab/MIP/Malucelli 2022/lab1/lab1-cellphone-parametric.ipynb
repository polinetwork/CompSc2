{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "ab919113",
   "metadata": {},
   "source": [
    "# Cell phone MIP problem in parametric form\n",
    "\n",
    "This notebook shows how to model and solve the cell phone factory production MIP using a more parametric approach, i.e., a general one where data could be read from a file."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "aedd867f",
   "metadata": {},
   "source": [
    "## Mobile phone factory\n",
    "\n",
    "The XYZ mobile corporation produces two models of cell phone, which we'll call M1 and M2, from a pool of components. Both M1 and M2 have the basic components (display, memory, camera, CPU), while only one model, M1, has two extras: thermal FLIR camera and satellite rx/tx.\n",
    "\n",
    "The only factory of XYZ has a limited supply of each component, and the number of components for each model is described in the following table\n",
    "\n",
    "Component|M1|M2|Availability\n",
    "---------|--|--|------------\n",
    "Display|1|2|10\n",
    "Memory|2|2|18\n",
    "Camera|1|3|12\n",
    "CPU|2|3|21\n",
    "Thermal cam.|1|0|9\n",
    "Satellite rx/tx|1|0|10\n",
    "\n",
    "The sales price for M1 is 110, and for M2 it is 130. Formulate the problem of finding how many models to produce of M1 and M2 in order to mazimize the total revenue.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "54577bfb",
   "metadata": {},
   "source": [
    "This problem can be modeled in a simple way. First, the main decision consists in two quantities: the number of M1 and the number of M2 to produce. We assign two variables $x_1$ and $x_2$ to these quantities.\n",
    "\n",
    "Next, the optimization model will have $110 x_1 + 130 x_2$ as objective function, which should be maximized. Finally, the constraints are given by each scarse resource (displays, memories, etc.). One constraint can be given for each resource. For instance, given that there are 10 displays in total and M1 uses one while M2 uses two, this implies the constraint\n",
    "\n",
    "$$\n",
    "x_1 + 2x_2 \\le 10\n",
    "$$\n",
    "\n",
    "And similarly for all other resources. The two variables $x_1$ and $x_2$ must obviously be nonnegative and integer. The final model can be written as follows:\n",
    "\n",
    "$$\n",
    "\\begin{array}{llll}\n",
    "\\max & 110 x_1 + 130 x_2\\\\\n",
    "\\textrm{s.t.} &   x_1 + 2 x_2 & \\le 10&\\qquad\\textrm{(display)}\\\\\n",
    "              & 2 x_1 + 2 x_2 & \\le 18&\\qquad\\textrm{(memory)}\\\\\n",
    "              &   x_1 + 3 x_2 & \\le 12&\\qquad\\textrm{(camera)}\\\\\n",
    "              & 2 x_1 + 3 x_2 & \\le 21&\\qquad\\textrm{(CPU)}\\\\\n",
    "              &   x_1         & \\le 9 &\\qquad\\textrm{(thermal camera)}\\\\\n",
    "              &   x_2         & \\le 10&\\qquad\\textrm{(sat. rx/tx)}\\\\\n",
    "              & x_1, x_2 \\in \\mathbb Z_+.\n",
    "\\end{array}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "965efb8d",
   "metadata": {},
   "source": [
    "In matricial form, one can define\n",
    "\n",
    "$$\n",
    "x = \\left(\\begin{array}{l}x_1\\\\x_2\\end{array}\\right);\\qquad\n",
    "c = \\left(\\begin{array}{r}110\\\\130\\end{array}\\right);\\qquad\n",
    "A = \\left(\\begin{array}{rr}1&2\\\\2&2\\\\1&3\\\\2&3\\\\1&0\\\\1&0\\\\\\end{array}\\right);\\qquad\n",
    "b = \\left(\\begin{array}{r}10\\\\18\\\\12\\\\21\\\\9\\\\10\\\\\\end{array}\\right),\n",
    "$$\n",
    "\n",
    "and the model can be re-written equivalently as\n",
    "\n",
    "$$\n",
    "\\begin{array}{lll}\n",
    "\\max          & c^T x\\\\\n",
    "\\textrm{s.t.} & A x \\le b\\\\\n",
    "              & x \\in \\mathbb Z_+^2\n",
    "\\end{array}\n",
    "$$\n",
    "\n",
    "We are ready to create an optimization problem using the `mip` module. We write a more general version where the data is specified separately, so that it could even be read from a file. Note that the component usage can be created as a 6x2 \"matrix\" simply by creating a list of six elements, each being a list of two. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "d59f6107",
   "metadata": {},
   "outputs": [],
   "source": [
    "import mip\n",
    "\n",
    "n_model = 2\n",
    "n_component = 6\n",
    "\n",
    "c = [110, 130]\n",
    "\n",
    "A = [[1,2],\n",
    "     [2,2],\n",
    "     [1,3],\n",
    "     [1,3],\n",
    "     [2,3],\n",
    "     [1,0],\n",
    "     [1,0]]\n",
    "\n",
    "b = [10,18,12,21,9,10]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "12b0156a",
   "metadata": {},
   "source": [
    "Now we create an empty model and add the two integer variables $[x_1,x_2]$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "56a4c1a9",
   "metadata": {},
   "outputs": [],
   "source": [
    "m = mip.Model()\n",
    "x = [m.add_var(\"number of produced cellphones of type \" + str(i), var_type=mip.INTEGER) for i in range(n_model)]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dbe45631",
   "metadata": {},
   "source": [
    "Let's write the objective function: in the general case, it can be written as a sum over the set of models."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "10e6dfaf",
   "metadata": {},
   "outputs": [],
   "source": [
    "m.objective = mip.maximize(mip.xsum(c[i]*x[i] for i in range(n_model)))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6bb56c88",
   "metadata": {},
   "source": [
    "The constraints can be generated in a loop:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "057865a6",
   "metadata": {},
   "outputs": [],
   "source": [
    "for j in range(n_component):\n",
    "    m.add_constr(mip.xsum(A[j][i]*x[i] for i in range(n_model)) <= b[j])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "54959656",
   "metadata": {},
   "source": [
    "The model is complete. Let's solve it and print the optimal solution."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "14dbf2db",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Welcome to the CBC MILP Solver \n",
      "Version: devel \n",
      "Build Date: Dec 22 2022\n",
      "Starting solution of the Linear programming relaxation problem using Primal Simplex\n",
      "\n",
      "Clp0024I Matrix will be packed to eliminate 1 small elements\n",
      "Coin0506I Presolve 4 (-2) rows, 2 (0) columns and 8 (-3) elements\n",
      "Clp1000I sum of infeasibilities 0 - average 0, 1 fixed columns\n",
      "Coin0506I Presolve 0 (-4) rows, 0 (-2) columns and 0 (-8) elements\n",
      "Clp0000I Optimal - objective value 495\n",
      "Clp0000I Optimal - objective value 495\n",
      "Coin0511I After Postsolve, objective 495, infeasibilities - dual 0 (0), primal 0 (0)\n",
      "Clp0000I Optimal - objective value 495\n",
      "Clp0000I Optimal - objective value 495\n",
      "Clp0000I Optimal - objective value 495\n",
      "Coin0511I After Postsolve, objective 495, infeasibilities - dual 0 (0), primal 0 (0)\n",
      "Clp0032I Optimal objective 495 - 0 iterations time 0.002, Presolve 0.00, Idiot 0.00\n",
      "\n",
      "Starting MIP optimization\n",
      "maxSavedSolutions was changed from 1 to 10\n",
      "Continuous objective value is 495 - 7.1e-05 seconds\n",
      "Cgl0004I processed model has 2 rows, 2 columns (2 integer (0 of which binary)) and 4 elements\n",
      "Coin3009W Conflict graph built in 0.001 seconds, density: 0.000%\n",
      "Cgl0015I Clique Strengthening extended 0 cliques, 0 were dominated\n",
      "Cbc0045I Cutoff increment increased from 0.0001 to 9.9999\n",
      "Cbc0012I Integer solution of 440 found by DiveCoefficient after 0 iterations and 0 nodes (0.01 seconds)\n",
      "Cbc0012I Integer solution of 460 found by DiveCoefficient after 0 iterations and 0 nodes (0.01 seconds)\n",
      "Cbc0013I At root node, 0 cuts changed objective from 483.33333 to 460 in 2 passes\n",
      "Cbc0014I Cut generator 0 (Probing) - 0 row cuts average 0.0 elements, 1 column cuts (1 active)  in 0.000 seconds - new frequency is 1\n",
      "Cbc0014I Cut generator 1 (Gomory) - 0 row cuts average 0.0 elements, 0 column cuts (0 active)  in 0.000 seconds - new frequency is -100\n",
      "Cbc0014I Cut generator 2 (Knapsack) - 0 row cuts average 0.0 elements, 0 column cuts (0 active)  in 0.000 seconds - new frequency is -100\n",
      "Cbc0014I Cut generator 3 (Clique) - 0 row cuts average 0.0 elements, 0 column cuts (0 active)  in 0.000 seconds - new frequency is -100\n",
      "Cbc0014I Cut generator 4 (OddWheel) - 0 row cuts average 0.0 elements, 0 column cuts (0 active)  in 0.000 seconds - new frequency is -100\n",
      "Cbc0014I Cut generator 5 (MixedIntegerRounding2) - 0 row cuts average 0.0 elements, 0 column cuts (0 active)  in 0.000 seconds - new frequency is -100\n",
      "Cbc0014I Cut generator 6 (FlowCover) - 0 row cuts average 0.0 elements, 0 column cuts (0 active)  in 0.000 seconds - new frequency is -100\n",
      "Cbc0001I Search completed - best objective 460, took 0 iterations and 0 nodes (0.01 seconds)\n",
      "Cbc0035I Maximum depth 0, 1 variables fixed on reduced cost\n",
      "Cuts at root node changed objective from 483.333 to 460\n",
      "Probing was tried 2 times and created 1 cuts of which 0 were active after adding rounds of cuts (1.9e-05 seconds)\n",
      "Gomory was tried 2 times and created 0 cuts of which 0 were active after adding rounds of cuts (1.4e-05 seconds)\n",
      "Knapsack was tried 2 times and created 0 cuts of which 0 were active after adding rounds of cuts (7e-06 seconds)\n",
      "Clique was tried 2 times and created 0 cuts of which 0 were active after adding rounds of cuts (3e-06 seconds)\n",
      "OddWheel was tried 2 times and created 0 cuts of which 0 were active after adding rounds of cuts (3e-06 seconds)\n",
      "MixedIntegerRounding2 was tried 2 times and created 0 cuts of which 0 were active after adding rounds of cuts (1e-05 seconds)\n",
      "FlowCover was tried 2 times and created 0 cuts of which 0 were active after adding rounds of cuts (6e-06 seconds)\n",
      "TwoMirCuts was tried 1 times and created 0 cuts of which 0 were active after adding rounds of cuts (9e-06 seconds)\n",
      "ZeroHalf was tried 1 times and created 0 cuts of which 0 were active after adding rounds of cuts (1e-06 seconds)\n",
      "\n",
      "Result - Optimal solution found\n",
      "Objective value:                460\n",
      "Enumerated nodes:               0\n",
      "Total iterations:               0\n",
      "Time (CPU seconds):             0.001409\n",
      "Time (Wallclock seconds):       0.00685596\n",
      "Total time (CPU seconds):       0.001543   (Wallclock seconds):       0.008955\n",
      "####################################################################################\n",
      "Optimization status: OptimizationStatus.OPTIMAL\n",
      "[3.0, 1.0]\n",
      "460.0\n"
     ]
    }
   ],
   "source": [
    "import time\n",
    "optimization_status = m.optimize(max_seconds=10)\n",
    "\n",
    "print(\"####################################################################################\")\n",
    "print(\"Optimization status: \" + str(optimization_status))\n",
    "# Print the value (.x) of each variable\n",
    "print([x[i].x for i in range(n_model)])\n",
    "# Print the objective function value\n",
    "print(m.objective_value)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.9"
  },
  "vscode": {
   "interpreter": {
    "hash": "b0fa6594d8f4cbf19f97940f81e996739fb7646882a419484c72d19e05852a7e"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
