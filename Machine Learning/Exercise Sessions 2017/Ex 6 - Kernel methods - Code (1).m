clear
clc
close all

% Load data
load iris_dataset;
%irisInputs = zscore(irisInputs);
irisInputs = irisInputs';

% Performing pca
[loadings, scores, variance] = pca(irisInputs);
cumsum(variance) / sum(variance)

loadings

pc1 = scores(:,1);
pc2 = scores(:,2);
pc3 = scores(:,3);
pc4 = scores(:,4);

%% Feature extraction
irisT = [ones(50,1); 2*ones(100,1)];
model_all = mnrfit(irisInputs, irisT);
model_pca = mnrfit([pc1 pc2], irisT);
model_acp = mnrfit([pc3 pc4], irisT);


prob_all = mnrval(model_all, irisInputs);
class_all = 1 * (prob_all(:,1) > prob_all(:,2)) + ...
    2 * (prob_all(:,1) <= prob_all(:,2));
sum(class_all == irisT)

prob_pca = mnrval(model_pca, [pc1 pc2]);
class_pca = 1 * (prob_pca(:,1) > prob_pca(:,2)) + ...
    2 * (prob_pca(:,1) <= prob_pca(:,2));
sum(class_pca == irisT)

prob_acp = mnrval(model_acp, [pc3 pc4]);
class_acp = 1 * (prob_acp(:,1) > prob_acp(:,2)) + ...
    2 * (prob_acp(:,1) <= prob_acp(:,2));
sum(class_acp == irisT)


%% Visualization
figure();
gscatter(pc1, pc2, irisT,'bg','..')
figure();
gscatter(pc3, pc4, irisT,'bg','..')

%% Compression capabilites of PCA
m = 4

x_zip = scores(:,1:m);
needed_loadings = loadings(:,1:m);
mean_values = mean(irisInputs);

x_rec = x_zip * needed_loadings' + repmat(mean_values, 150, 1);

mean((irisInputs - x_rec).^2)

figure();
xlim([4.3 7.9]);
ylim([2 4.4]);
for ii = 1:150
    hold on;
    plot([irisInputs(ii,1) x_rec(ii,1)],[irisInputs(ii,2) x_rec(ii,2)],'k');
end
xlabel('x_1')
ylabel('x_2')